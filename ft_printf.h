/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/17 14:31:39 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/17 15:20:38 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

#include "libft/libft.h"
#include <stdarg.h>

int		print_params(char *mod, va_list list);
char	*if_statement(char *mod, char *str);
char	*percent_check(char *str, int *count);
int		print_srting(char *str, va_list list, char **mods);
int		ft_printf(const char *str, ...);
char	get_char_params(va_list list);
char	*get_str_params(va_list list);
int		get_int_params(va_list list);
int		get_unsigned_params(va_list list);
void	*get_void_params(va_list list);
int		is_printf(char c);
char	**split_str(const char *str);
char	*check_mods(const char *str);
char	*temp_check(char *split, char *temp);
char	**get_mods(char **split);
int		find_zero(char *str);
int		print_zero(int zero, int num);
int		is_flag(char c);
int		find_space(char *str);
int		print_space(int num, char *str);
int		print_char_params(va_list list, char *mod);
int		print_str_params(va_list list, char *mod);
int		print_int_params(va_list list, char *mod);
int		print_unsigned_params(va_list list, char *mod);
int		print_other_params(va_list list, char *mod);

#endif
