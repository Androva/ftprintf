# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/09/17 14:28:10 by nmatutoa          #+#    #+#              #
#    Updated: 2018/09/17 15:19:37 by nmatutoa         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libftprintf.a
FLAGS = gcc -Wall -Werror -Wextra -c
SOURCES = get_params.c is_printf.c is_flag.c mods.c zero_flag.c space_flag.c ft_printf.c

OBJECTS = get_params.o is_printf.o is_flag.o mods.o zero_flag.o space_flag.o ft_printf.o

all: $(NAME)

$(NAME): $(OBJECTS)
	@make -C libft
	@ar rc $(NAME) $(OBJECTS)
	@ranlib $(NAME)
	@echo "libftprintf.a created"

OBJECTS:
	@$(FLAGS) $(SOURCES) ft_printf.h
	@echo "libftprintf compiled"

clean:
	@make clean -C libft
	@/bin/rm -f $(OBJECTS)
	@echo "Object files removed"

fclean: clean
	@make fclean -C libft
	@/bin/rm -f $(NAME)
	@echo "$(NAME) removed"

re: fclean all
